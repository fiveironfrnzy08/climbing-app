import './App.css';
import axios from 'axios';
import {useEffect, useRef, useState} from 'react';
import PuffLoader from 'react-spinners/PuffLoader';

const CLIMBING_URL_KEY = 'climbing_url';
const SERVER_URL = '';
const POLL_INTERVAL = 10000;

function daysInThisMonth() {
  const now = new Date();
  return new Date(now.getFullYear(), now.getMonth()+1, 0).getDate();
}

function App() {
  const [url, setUrl] = useState(localStorage.getItem(CLIMBING_URL_KEY));
  const [polling, setPolling] = useState(!!localStorage.getItem(CLIMBING_URL_KEY));
  const [title, setTitle] = useState('');
  const [loading, setLoading] = useState(false);
  const [openSlots, setOpenSlots] = useState([]);
  const [selectedDay, setSelectedDay] = useState(new Date().getDate());
  const lastNotified = useRef({});
  const interval = useRef(null);

  useEffect(() => {
    localStorage.setItem(CLIMBING_URL_KEY, url);
    if(!('Notification' in window)) {
      console.log('This browser does not support desktop notification');
    } else {
      Notification.requestPermission();
    }
    if(interval.current) {
      window.clearInterval(interval.current);
    }
    if(polling && url) {
      const getOpenSlots = () => {
        setLoading(true);
        axios.post(SERVER_URL, {url, day: selectedDay}).then((response) => {
          setOpenSlots(response.data.slots.filter((item) => item.capacity > 0));
          setTitle(`${response.data.title} on ${response.data.day}`);
        }).catch((error) => {
          window.clearInterval(interval.current);
          return new window.Notification('An error occurred trying to get the time slots. Refresh the page and start over.');
        }).finally(() => setLoading(false));
      };
      getOpenSlots();
      interval.current = setInterval(getOpenSlots, POLL_INTERVAL);
    } else {
      interval.current = null;
    }
  }, [polling, url, selectedDay]);
  useEffect(() => {
    openSlots.forEach((item) => {
      if(lastNotified.current[item.time] !== item.capacity) {
        new window.Notification(`${item.capacity} at ${item.time}`, {
          tag: item.time,
          requireInteraction: true,
          renotify: false,
        });
        lastNotified.current[item.time] = item.capacity;
      }
    });
  }, [openSlots]);
  return (
    <div className="App">
      <header className="App-header">
        Find the link to the page that shows the time slots and capacity
        <a className="App-link" href="https://www.earthtreksclimbing.com/covid-reopening-information-colorado/"
           target="_blank"
           rel="noreferrer">Reservation
          Links
        </a>
        <br/>
        <div style={{display: 'flex'}}>
          <label htmlFor="url">URL:</label>
          <input id="url" defaultValue={url} onBlur={(e) => setUrl(e.target.value)} style={{width: 300}}/>
          <button style={{marginLeft: 12}} disabled={polling} onClick={() => setPolling(true)}>Start</button>
          <button style={{marginLeft: 12}} disabled={!polling} onClick={() => setPolling(false)}>Stop</button>
        </div>
        <div style={{display: 'flex'}}>
          <label htmlFor="url">Day:</label>
          <input id="day" type="number" min={new Date().getDate()} max={daysInThisMonth()} defaultValue={new Date().getDate()}
                 onChange={(e) => setSelectedDay(e.target.value)}
                 style={{width: 50}}/>
        </div>
        <br/>
        {polling && title && (
          <>
            <div>
              {title}:&nbsp;
              <PuffLoader color="#36D7B7" loading={true} css="" size={24}/>
            </div>
            {openSlots.length > 0 ? openSlots.map((item) => (
              <a key={item.time} className="App-link" href={url} target="_blank" rel="noreferrer">
                {item.capacity} slots open from {item.time.split(',')[2]}
              </a>)) : 'No open slots :('}
          </>
        )}
        {polling && !loading && url && !title && (
          <div>
            Double check the url. The page should show something like this:
            <img src={process.env.PUBLIC_URL + 'schedule.png'} width={500}/>
          </div>
        )}
      </header>
    </div>
  );
}

export default App;

const puppeteer = require('puppeteer');
const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const app = express();

const PORT = process.env.PORT || 5000;

app.use(express.static(path.join(__dirname, 'client/build')));

app.use(bodyParser());

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);

  next();
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

app.post('/', function (req, res) {
  (async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const day = req.body.day;
    console.log(req.body);
    await page.goto(req.body.url);
    const data = await page.evaluate((dayVal) => {
      let day = document.querySelector('.ui-datepicker-calendar td a.ui-state-active').innerText;
      if (dayVal && Number(day) !== dayVal) {
        [...document.querySelectorAll('.ui-datepicker-calendar td')].find((el) => el.innerText === dayVal)?.click();
      }
      return new Promise((resolve) => {
        setTimeout(() => {
          const rows = [...document.querySelectorAll('#offering-page-select-events-table tr')];
          const title = document.querySelector('.header-strip-with-event-title span')?.innerHTML?.replaceAll('\n', '')?.trim();
          day = document.querySelector('.ui-datepicker-calendar td a.ui-state-active').innerText;
          resolve({
            title,
            day,
            slots: rows.map(x => {
              const time = x.querySelectorAll('td')[0].innerHTML.replaceAll('\n', '');
              const capacity = x.querySelector('.offering-page-event-is-full') ? 0 : parseInt(x.querySelectorAll('td')[1].textContent.replaceAll('\n', '').replace(' space', '').replace('Availability', ''));
              return ({
                time,
                capacity
              });
            }),
          });
        }, 400);
      });
    }, day);

    console.log(data);
    res.send(data);

    await browser.close();
  })();
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`)
});

